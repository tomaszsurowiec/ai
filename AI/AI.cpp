// AI.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <time.h>

double funkcja(int x);

int wspinaczka(int x, int zakres_od, int zakres_do);

int losuj(int modulo);

int gen(int zakres_od, int zakres_do, int *tab, int n, int ujemne_tez);
int ustal_n(int x);
void rodzice(int *tab, int n);

int dwa_w_dziesiec(int *tab, int n, int minus);
void dziesiec_w_dwa(int *tab, int n, int minus);

void kopiuj(int *tab1, int *tab2, int n);

using namespace std;
int main()
{
	int zakres_od;
	int zakres_do;
	int wylos = 0;
	int n;
	int ujemne_tez = 0;

	int max;
	int max_calk = 0;
	int i = 0;
	int j = 0;
	int wyniki[2][1000];
	int statystyki[2][8];
	

	int *bin;

	srand(time(NULL));

	cout << "Podaj zakres w jakim ma byc badana funkcja:" << endl<<"Od: ";

	cin >> zakres_od;

	cout << "Do: ";
	cin >> zakres_do;
	while (i < 1000)
	{
		if (zakres_od < zakres_do)
		{
			if (zakres_od < 0 && zakres_do < 0)
			{
				wylos = losuj(((zakres_od)*(-1)) - (zakres_do*(-1)))*(-1);
				n = ustal_n((-1)*zakres_od) + 1;
				ujemne_tez = 1;
			}
			else if (zakres_od < 0)
			{
				wylos = losuj(((zakres_od)*(-1)) + zakres_do) - (zakres_od*(-1));
				if ((zakres_od * (-1)) > zakres_do)
				{
					n = ustal_n((-1)*zakres_od) + 1;
					ujemne_tez = 1;
				}
				else
				{
					n = ustal_n(zakres_do);
					ujemne_tez = 1;
				}
			}
			else
			{
				wylos = losuj(zakres_do - zakres_od) + zakres_od;
				n = ustal_n(zakres_do);
			}

		}
		else if (zakres_od > zakres_do)
		{
			printf("B��dny zakres...");
			return 0;
		}
		else
		{
			wylos = zakres_od;
			if (zakres_od < 0)
			{
				n = ustal_n((-1) * zakres_od) + 1;
				ujemne_tez = 1;
			}
			else
			{
				n = ustal_n(zakres_od);
			}
		}

		max = wspinaczka(wylos, zakres_od, zakres_do);
		wyniki[0][i] = max;
		if (i == 0)
			max_calk = max;
		else if (funkcja(max)>funkcja(max_calk))
			max_calk = max;

		cout << "algorytm wspinaczkowy: x=" << max << " f(x)="<<funkcja(max)<<endl;

		bin = new int[10 * n];
		rodzice(bin, 10 * n);
		max = gen(zakres_od, zakres_do, bin, n, ujemne_tez);
		wyniki[1][i] = max;
		if (funkcja(max)>funkcja(max_calk) && max < zakres_do && max > zakres_od)
			max_calk = max;

		cout << "algorytm genetyczny: " << max << " f(x)=" << funkcja(max) << endl;

		i++;
	}

	cout << "najwieksza watosc jest dla: x=" << max_calk << " f(x)=" << funkcja(max_calk)<< endl;

	i = 0;
	while (i < 1000)
	{
		j = 0;
		while (j < 8)
		{
			statystyki[0][j] = 0;
			statystyki[1][j] = 0;
			j++;
		}
		i++;
	}

	i = 0;
	while (i < 1000)
	{
		if (wyniki[0][i]<zakres_od || wyniki[0][i]>zakres_do)
		{
			statystyki[0][0]++;	//wynik z poza zakresu alg. wspinaczkowy
		}
		else if (funkcja(wyniki[0][i]) == funkcja(max_calk))
		{
			statystyki[0][1]++;	//wynik identyczny alg. wspinaczkowy
		}
		else if (funkcja(max_calk) - funkcja(wyniki[0][i]) <= 1)
		{
			statystyki[0][2]++; //wynik mniejszy o max 1 alg. wspinaczkowy
		}
		else if (funkcja(max_calk) - funkcja(wyniki[0][i]) <= 5)
		{
			statystyki[0][3]++; //wynik mniejszy o max 5 alg. wspinaczkowy
		}
		else if (funkcja(max_calk) - funkcja(wyniki[0][i]) <= 10)
		{
			statystyki[0][4]++; //wynik mniejszy o max 10 alg. wspinaczkowy
		}
		else if (funkcja(max_calk) - funkcja(wyniki[0][i]) <= 100)
		{
			statystyki[0][5]++; //wynik mniejszy o max 100 alg. wspinaczkowy
		}
		else if (funkcja(max_calk) - funkcja(wyniki[0][i]) <= 1000)
		{
			statystyki[0][6]++; //wynik mniejszy o max 1000 alg. wspinaczkowy
		}
		else if (funkcja(max_calk) - funkcja(wyniki[0][i]) > 1000)
		{
			statystyki[0][7]++; //wynik mniejszy o > 1000 alg. wspinaczkowy
		}

		if (wyniki[1][i]<zakres_od || wyniki[1][i]>zakres_do)
		{
			statystyki[1][0]++;	//wynik z poza zakresu alg. genetyczny
		}
		else if (funkcja(wyniki[1][i]) == funkcja(max_calk))
		{
			statystyki[1][1]++; //wynik identyczny alg. genetyczny
		}
		else if (funkcja(max_calk) - funkcja(wyniki[1][i]) <= 1)
		{
			statystyki[1][2]++;	//wynik mniejszy o max 1 alg. genetyczny
		}
		else if (funkcja(max_calk) - funkcja(wyniki[1][i]) <= 5)
		{
			statystyki[1][3]++;	//wynik mniejszy o max 5 alg. genetyczny
		}
		else if (funkcja(max_calk) - funkcja(wyniki[1][i]) <= 10)
		{
			statystyki[1][4]++;	//wynik mniejszy o max 10 alg. genetyczny
		}
		else if (funkcja(max_calk) - funkcja(wyniki[1][i]) <= 100)
		{
			statystyki[1][5]++;	//wynik mniejszy o max 100 alg. genetyczny
		}
		else if (funkcja(max_calk) - funkcja(wyniki[1][i]) <= 1000)
		{
			statystyki[1][6]++;	//wynik mniejszy o max 1000 alg. genetyczny
		}
		else if (funkcja(max_calk) - funkcja(wyniki[1][i]) > 1000)
		{
			statystyki[1][7]++;	//wynik mniejszy o > 1000 alg. genetyczny
		}

		i++;
	}

	cout << "Statystyki:" << endl << "Na 1000 wynikow z kazdego z algorytmow:" << endl;
	cout << endl << "Algorytm wspinaczkowy mial wynikow o roznicy:" << endl;
	cout << "Poza zakresem : " << statystyki[0][0] << endl;
	cout << "Dokladnie tyle samo : " << statystyki[0][1] << endl;
	cout << "<= 1 : " << statystyki[0][2] << endl;
	cout << "<= 5 : " << statystyki[0][3] << endl;
	cout << "<= 10 : " << statystyki[0][4] << endl;
	cout << "<= 100 : " << statystyki[0][5] << endl;
	cout << "<= 1000 : " << statystyki[0][6] << endl;
	cout << ">= 1000 : " << statystyki[0][7] << endl;
	cout << endl << "Algorytm genetyczny mial wynikow o roznicy:" << endl;
	cout << "Poza zakresem : " << statystyki[1][0] << endl;
	cout << "Dokladnie tyle samo : " << statystyki[1][1] << endl;
	cout << "<= 1 : " << statystyki[1][2] << endl;
	cout << "<= 5 : " << statystyki[1][3] << endl;
	cout << "<= 10 : " << statystyki[1][4] << endl;
	cout << "<= 100 : " << statystyki[1][5] << endl;
	cout << "<= 1000 : " << statystyki[1][6] << endl;
	cout << ">= 1000 : " << statystyki[1][7] << endl;

	getchar();
	getchar();

    return 0;
}

double funkcja(int x)
{
	double f = pow(((double)x)/8.0,2.0) - pow(((double)x)/10.0,3.0) + pow(((double)x)/18.0,4.0) + ((double)x)/2.0;
		//(x/8)^2 - (x/10)^3 + (x/18)^4 + (x/2)

	return f;
}

int losuj(int modulo)
{
	int los;
	los = (rand() % modulo);

	return los;
}

int wspinaczka(int x, int zakres_od, int zakres_do)
{
	int max = x;
	
	int i;
	double temp = x;
	int skok = 1;

	while (temp >= zakres_od && temp <= zakres_do)
	{
		if (funkcja(temp + skok) > funkcja(temp))
		{
			max = temp;
			temp += skok;	
		}
		else if (funkcja(temp - skok) > funkcja(temp))
		{
			max = temp;
			temp -= skok;
		}
		else
			break;
	}

	return max;
}

int ustal_n(int x)	//x musi by� dodatnie, �eby to mia�o sens
{
	int i = 0;
	int temp = x;

	while (temp > 1)
	{
		temp /= 2;
		i++;
	}

	return i + 2;
}

void rodzice(int *tab, int n)
{
	int i = 0;
	while (i < n)
	{
		*(tab + i) = losuj(2);
		i++;
	}
}

int dwa_w_dziesiec(int *tab, int n, int minus)
{
	int numer = 0;
	int i = 0;
	while (i < n)
	{
		if (minus == 1)
		{
			if (i != 0)
				numer += *(tab + i)*pow(2, i - 1);
		}
		else
			numer += *(tab + i)*pow(2, i);
		i++;
	}
	if (minus == 1 && *tab == 1)
		numer *= (-1);
	return numer;
}

void dziesiec_w_dwa(int *tab, int n, int minus)
{
	//potrzebne?
}

int gen(int zakres_od, int zakres_do, int *tab, int n, int ujemne_tez)
{
	int max = 0;
	int pierwszy = 0;
	int ocen[10];
	int *tab2 = new int[10 * n];
	int krzyz, krzyz2;
	int i = 0;
	int j = 0;
	int zera = 0;

	max = dwa_w_dziesiec(tab, n, ujemne_tez);

	int number;
	int powtorka = 0;
	while (powtorka < 100)
	{
		i = 0;
		while (i < 10)
		{
			number = dwa_w_dziesiec(tab + (i * n), n, ujemne_tez);
			if (number<zakres_od || number > zakres_do)
			{
				ocen[i] = 0;
				zera++;
			}
			else
			{
				ocen[i] = 1;
				if (pierwszy == 0)
				{
					max = number;
					pierwszy = 1;
				}
				else if (funkcja(number) > funkcja(max))
					max = number;
			}
			i++;
		}

		i = 0;

		while (i < 10 - zera)
		{
			if (zera == 9)
			{
				j = 0;
				krzyz = -1;
				while (j < n && krzyz == -1)
				{
					if (ocen[j] == 1)
					{
						krzyz = j;
						ocen[j]--;
					}
					j++;
				}
				kopiuj(tab2 + (i * n), tab + (krzyz * n), n);
				kopiuj(tab2 + (i + 1 * n), tab + (krzyz * n), n);
				if (*(tab + (krzyz * n) + n - 1) == 1)
					*(tab2 + (i + 1 * n) + n - 1) = 0;
				else
					*(tab2 + (i + 1 * n) + n - 1) = 1;
			}
			else
			{
				j = 0;
				krzyz = -1;
				krzyz2 = -1;
				while (j < 10 && krzyz == -1)
				{
					if (ocen[j] == 1 && krzyz == -1)
					{
						krzyz = j;
						ocen[j]--;
					}
					j++;
				}
				j = 0;
				while (j < 10 && krzyz2 == -1)
				{
					if (ocen[j] == 1 && krzyz2 == -1 && j != krzyz)
					{
						krzyz2 = j;
						ocen[j]--;
					}
					j++;
				}
				if (krzyz2 == -1)
				{
					kopiuj(tab2 + (i * n), tab + (krzyz * n), n);
					kopiuj(tab2 + (i + 1 * n), tab + (krzyz * n), n);
					if (*(tab + (krzyz * n) + n - 1) == 1)
						*(tab2 + (i + 1 * n) + n - 1) = 0;
					else
						*(tab2 + (i + 1 * n) + n - 1) = 1;
				}
				else
				{
					kopiuj(tab2 + (i * n), tab + (krzyz * n), n / 2);
					kopiuj(tab2 + (i * n) + n / 2, tab + (krzyz2 * n) + n / 2, n - (n / 2));
					kopiuj(tab2 + ((i + 1) * n), tab + (krzyz2 * n), n / 2);
					kopiuj(tab2 + ((i + 1) * n) + n / 2, tab + (krzyz * n) + n / 2, n - (n / 2));
				}
			}

			i += 2;
		}

		while (i < 10)
		{
			krzyz = losuj(10);
			krzyz2 = losuj(10);
			kopiuj(tab2 + (i * n), tab + (krzyz * n), n / 2);
			kopiuj(tab2 + (i * n) + n / 2, tab + (krzyz2 * n) + n / 2, n - (n / 2));
			kopiuj(tab2 + ((i + 1) * n), tab + (krzyz2 * n), n / 2);
			kopiuj(tab2 + ((i + 1) * n) + n / 2, tab + (krzyz * n) + n / 2, n - (n / 2));
			

			i += 2;
		}

		kopiuj(tab, tab2, n * 10);
		powtorka++;
	}
	return max;
}

void kopiuj(int *tab1, int *tab2, int n)
{
	int i = 0;
	while (i < n)
	{
		*(tab1 + i) = *(tab2 + i);
		i++;
	}
}